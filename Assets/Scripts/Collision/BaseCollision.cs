﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SceneObjectType
{
    Soul, // 氣魂, 角色行動所需的元素
    Wall, // 無特殊效果的牆
    PlayerAvatar, // 玩家頭像
    EnemyAvatar, // 敵人頭像
    HugeRock, // 巨石
    BrokenRock, // 碎石
    BuffArea, // 八卦陣
    DebuffArea, // 沼地
    GameItem, // 圖騰及道具
}

public enum SoulType
{
    None,
    Penetration, // 穿透
    Floating, // 浮空
    Explosion, // 爆破
    Shield, // 能量
}

public enum GameItemType
{
    None = -1,
    FistToken, // 拳圖騰
    LegToken, // 腳圖騰
    HumanToken, // 人圖騰
    SwordToken, // 劍圖騰
    Bun, // 包子
    Pill, // 藥丸
}

public class BaseCollision : MonoBehaviour
{
    public SceneObjectType objectType;
    public SoulType soulType;
    public GameItemType gameItemType = GameItemType.None;

    Rigidbody2D rb;
    Vector2 lastVelocity;

    bool statusChanged;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (rb != null)
        {
            lastVelocity = rb.velocity;


            if (this.gameObject == GamePlay_Demo2.Instance.NowSelectedSoul |
            this.gameObject == GamePlay_Demo2.Instance.nowEnemySelectedSoul)
            {
                if ((GamePlay_Demo2.Instance.NowGameStatus == GamePlay_Demo2.GameStatus.PlayerAttack ||
                    GamePlay_Demo2.Instance.NowGameStatus == GamePlay_Demo2.GameStatus.EnemyAttack) &&
                    lastVelocity.magnitude <= 0.01f)
                {
                    ChangeToNextGameStatus();
                }
            }
        }
    }

    private void OnDestroy()
    {
        ChangeToNextGameStatus();
    }

    void ChangeToNextGameStatus()
    {
        if (statusChanged)
            return;

        if (this.gameObject == GamePlay_Demo2.Instance.NowSelectedSoul)
            GamePlay_Demo2.Instance.SetStatusToEnemyAttack();
        else
            GamePlay_Demo2.Instance.SetStatusToPutGem();

        statusChanged = true;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        // 如果是被觸碰到的, 不主動做任何反應
        if (this.gameObject != GamePlay_Demo2.Instance.NowSelectedSoul &&
            this.gameObject != GamePlay_Demo2.Instance.nowEnemySelectedSoul)
            return;

        BaseCollision otherCollision = col.gameObject.GetComponent<BaseCollision>();

        if (otherCollision != null)
        {
            switch (otherCollision.objectType)
            {
                case SceneObjectType.Soul:
                    if (rb != null)
                    {
                        Rigidbody2D _rb = col.gameObject.GetComponent<Rigidbody2D>();
                        if (_rb != null)
                        {
                            _rb.velocity = rb.velocity;
                        }

                        rb.velocity = Vector2.zero;
                    }
                    break;
                case SceneObjectType.Wall:
                        float speed = lastVelocity.magnitude;
                        Vector2 direction = Vector3.Reflect(lastVelocity.normalized, col.contacts[0].normal);
                        rb.velocity = direction * Mathf.Max(speed, 0f);
                        ParticlesManager.Instance.CreatePSEmitter(ParticlesManager.collision, transform);
                    break;
                case SceneObjectType.PlayerAvatar:
                    if (this.gameObject == GamePlay_Demo2.Instance.nowEnemySelectedSoul)
                    {
                        GamePlay_Demo2.Instance.AddPlayerHP(-30);
                        Destroy(gameObject);
                    }
                    break;
                case SceneObjectType.EnemyAvatar:
                    if (this.gameObject == GamePlay_Demo2.Instance.NowSelectedSoul)
                    {
                        GamePlay_Demo2.Instance.AddEnemyHP(-30);
                        Destroy(gameObject);
                    }
                    break;
                case SceneObjectType.HugeRock:
                    Destroy(gameObject);
                    break;
                case SceneObjectType.BrokenRock:
                    Destroy(gameObject);
                    break;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        // 如果是被觸碰到的, 不主動做任何反應
        if (this.gameObject != GamePlay_Demo2.Instance.NowSelectedSoul &&
            this.gameObject != GamePlay_Demo2.Instance.nowEnemySelectedSoul)
            return;

        BaseCollision otherCollision = other.GetComponent<BaseCollision>();

        if (otherCollision != null)
        {
            switch (otherCollision.objectType)
            {
                case SceneObjectType.BuffArea:
                    break;
                case SceneObjectType.DebuffArea:
                    break;
                case SceneObjectType.GameItem:
                    Destroy(other.gameObject);
                    break;
            }
        }
    }
}
