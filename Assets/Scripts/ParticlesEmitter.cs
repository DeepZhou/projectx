﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticlesEmitter : MonoBehaviour
{
    public bool autoStart = true;

    public bool autoDestroy = true;

    ParticleSystem ps;

    // Start is called before the first frame update
    void Start()
    {
        ps = GetComponent<ParticleSystem>();

        if (autoStart)
            Play();
        else
            ps.Stop();
    }

    public void Play()
    {
        StartCoroutine(PlayThenCheckDestroySelf());
    }

    IEnumerator PlayThenCheckDestroySelf()
    {
        ps.Play();

        if (autoDestroy)
        {
            yield return new WaitForSeconds(ps.main.duration);
            Destroy(gameObject);
        }
    }
}
