﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BagPopup : MonoBehaviour
{
    public UIDragHandler dragHnd;

    public Button openBagButton;
    public Button closeBagButton;
    public GameObject bagGO;

    bool dragging = false;
    Vector2 startTouchPos;

    // Start is called before the first frame update
    void Start()
    {
        dragHnd.BeginDrag += DragBegin;
        dragHnd.EndDrag += DragEnd;
        InitButtonEvent();
    }

    void OnDestroy()
    {
        dragHnd.BeginDrag -= DragBegin;
        dragHnd.EndDrag -= DragEnd;
        DeinitButtonEvent();
    }

    void InitButtonEvent()
    {
        openBagButton.onClick.AddListener(OpenBagButtonPressed);
        closeBagButton.onClick.AddListener(CloseBagButtonPressed);
    }

    void DeinitButtonEvent()
    {
        openBagButton.onClick.RemoveListener(OpenBagButtonPressed);
        closeBagButton.onClick.RemoveListener(CloseBagButtonPressed);
    }

    public void OpenBagButtonPressed()
    {

    }

    public void CloseBagButtonPressed()
    {
        dragging = false;
        closeBagButton.gameObject.SetActive(false);
        bagGO.SetActive(false);
    }

    void DragBegin(Vector2 beingPos)
    {
        startTouchPos = beingPos;
    }

    void DragEnd(Vector2 endPos)
    {
        if ((endPos - startTouchPos).magnitude > 10f)
            dragging = true;
        else
            dragging = false;

        if (dragging)
            return;

        closeBagButton.gameObject.SetActive(true);
        bagGO.SetActive(true);
    }
}
