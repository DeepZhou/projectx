﻿using System.Threading.Tasks;
using UnityEngine;

public class AsyncFuncHandler
{
    public delegate void AsyncFunc(float deltaTime);

    public static async void DoAsyncFunc(float dura, AsyncFunc func)
    {
        await RunAsyncFunc(dura, func);
    }

    static async Task RunAsyncFunc(float dura, AsyncFunc func)
    {
        float deltaTime = 0f;

        while (deltaTime <= 1f)
        {
            deltaTime += Time.deltaTime / dura;
            func.Invoke(deltaTime);
            await Task.Yield();
        }
    }
}
