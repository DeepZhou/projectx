﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionContainer : MonoBehaviour
{
    public bool IsFull
    {
        get { return item != null; }
    }

    [SerializeField]
    private ActionItem item;

    public void SetActionItem(ActionItem item)
    {
        this.item = item;

        if (item != null)
            item.transform.position = transform.position;
    }

    public ActionItem GetActionItem()
    {
        return this.item;
    }
}
