﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteAnimationHandler : MonoBehaviour
{
    public Image target;
    public List<Sprite> spriteList;

    public int fps = 30;
    public bool DestroyWhenFinished = true;

    void Start()
    {
        if (target == null)
            target = GetComponent<Image>();
    }

    public void PlayAnimation(bool repeat = false)
    {
        if (target == null)
        {
            Debug.Log("target is null!, name: " + name);
            return;
        }

        StartCoroutine(UpdateSprite(repeat));
    }

    IEnumerator UpdateSprite(bool repeat)
    {
        float timePerFrame = 1f / (float)fps;
        Debug.Log("timePerFrame: " + timePerFrame);

        for (int i = 0; i < spriteList.Count; i++)
        {
            target.sprite = spriteList[i];
            yield return new WaitForSeconds(timePerFrame);

            if (repeat)
            {
                if (i == spriteList.Count - 1)
                {
                    if (repeat)
                        i = 0;
                }
            }
        }

        if (DestroyWhenFinished)
            Destroy(gameObject);
    }
}
