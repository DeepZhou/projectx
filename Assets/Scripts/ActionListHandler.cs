﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionListHandler : MonoBehaviour
{
    public static ActionListHandler Instance;

    List<ActionContainer> containerList;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        InitActionContainerList();
    }

    void InitActionContainerList()
    {
        if (containerList == null)
            containerList = new List<ActionContainer>();
        else
            containerList.Clear();

        foreach (var item in GetComponentsInChildren<ActionContainer>())
        {
            containerList.Add(item);
        }
    }
}
