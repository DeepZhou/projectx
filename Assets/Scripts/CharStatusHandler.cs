﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharStatusHandler : MonoBehaviour
{
    public enum BarDecreaseEffectType
    {
        Slow,
        Fast,
    }

    public Image HPBar;
    public Image HPDecreaseEffectBar;
    public Image MPBar;

    public bool isPlayerStatus;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void AddHPBarProgress(float prog)
    {
        // 因為最大fillamount為0.5f, 所以需要轉換
        float changeProg = prog * 0.5f;
        float nowProg = HPBar.fillAmount;
        nowProg += changeProg;
        
        if (nowProg > 0.5f)
            nowProg = 0.5f;
        else if (nowProg < 0f)
        {
            changeProg -= nowProg;
            nowProg = 0f;
        }

        HPBar.fillAmount = nowProg;

        if (changeProg < 0f)
            StartCoroutine(HPDecreaseEffect(changeProg, HPBar.fillAmount, BarDecreaseEffectType.Slow));
    }

    public void AddMPBarProgress(float prog)
    {
        float nowProg = MPBar.fillAmount * 2f;

        nowProg += prog;

        nowProg *= 0.5f; // 因為最大fillamount為0.5f, 所以需要轉換

        if (nowProg > 0.5f)
            nowProg = 0.5f;
        else if (nowProg < 0f)
            nowProg = 0f;

        MPBar.fillAmount = nowProg;
    }

    IEnumerator HPDecreaseEffect(float decreaseProg, float nowBarProg, BarDecreaseEffectType type)
    {
        float deltaTime = 0f;
        float totalEffectTime = 0f;
        decreaseProg = Mathf.Abs(decreaseProg);

        // 初始
        float rotAngle = -180f * (0.5f - nowBarProg) * 2f;
        Vector2 pos = (HPDecreaseEffectBar.transform as RectTransform).anchoredPosition;
        pos.y = 0f;
        (HPDecreaseEffectBar.transform as RectTransform).anchoredPosition = pos;
        Color c = HPDecreaseEffectBar.color;
        c.a = 1f;
        HPDecreaseEffectBar.color = c;
        HPDecreaseEffectBar.fillAmount = decreaseProg;
        HPDecreaseEffectBar.transform.eulerAngles = new Vector3(0f, 0f, rotAngle);
        HPDecreaseEffectBar.gameObject.SetActive(true);

        // 震動效果
        // float shockDura = 0.0f;
        // float shockValue = 10f;
        // deltaTime = 0f;
        // while (deltaTime <= shockDura)
        // {
        //     deltaTime += Time.deltaTime;
        //     Vector2 _pos = pos + new Vector2(Random.Range(-shockValue, shockValue), Random.Range(-shockValue, shockValue));
        //     (HPDecreaseEffectBar.transform as RectTransform).anchoredPosition = _pos;
        //     yield return null;
        // }

        if (type == BarDecreaseEffectType.Fast)
        {
            // 墜落淡出效果
            float dropFadeOutDura = 0.3f;
            float dropDist = 50f;
            deltaTime = 0f;
            while (deltaTime <= dropFadeOutDura)
            {
                deltaTime += Time.deltaTime;
                pos.y = Mathf.Lerp(0f, -dropDist, deltaTime / dropFadeOutDura);
                (HPDecreaseEffectBar.transform as RectTransform).anchoredPosition = pos;
                c.a = Mathf.Lerp(1f, 0f, deltaTime / dropFadeOutDura);
                HPDecreaseEffectBar.color = c;
                yield return null;
            }

            totalEffectTime += dropFadeOutDura;
        }
        else if (type == BarDecreaseEffectType.Slow)
        {
            // 緩慢減少效果
            float decreaseDelay = 0.5f;
            float decreaseAnimTime = 0.3f;
            yield return new WaitForSeconds(decreaseDelay);            
            deltaTime = 0f;
            while (deltaTime <= decreaseAnimTime)
            {
                deltaTime += Time.deltaTime;
                HPDecreaseEffectBar.fillAmount = Mathf.Lerp(decreaseProg, 0f, deltaTime / decreaseAnimTime);
                Debug.Log(HPDecreaseEffectBar.fillAmount);
                yield return null;
            }

            totalEffectTime += decreaseDelay + decreaseAnimTime;
        }

        yield return new WaitForSeconds(totalEffectTime);
        HPDecreaseEffectBar.gameObject.SetActive(false);
    }
}
