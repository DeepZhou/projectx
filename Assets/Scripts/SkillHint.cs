﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SkillHint : MonoBehaviour
{
    public Text skillNameText;
    public Text skilldamageText;
    public Text skillCostText;
    public List<Image> actionList;

    public Sprite[] actionSprite;

    public void Setup(string skillName, int damage, int cost, string action)
    {
        skillNameText.text = "招式名稱：" + skillName;
        skilldamageText.text = "傷害：" + damage;
        skillCostText.text = "內力消耗：" + cost;
        actionList.ForEach(img =>
        {
            img.gameObject.SetActive(false);
        });

        if (action.Length > 0)
        {
            for (int i = 0; i < action.Length; i++)
            {
                int idx = int.Parse(action[i].ToString());
                actionList[i].sprite = actionSprite[idx];
                actionList[i].gameObject.SetActive(true);
            }
        }
    }
}
