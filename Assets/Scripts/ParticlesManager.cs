﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesManager : MonoBehaviour
{
    public static ParticlesManager Instance = null;

    public ParticlesEmitter collisionPSPrefab;

    public const string collision = "collision";

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    public void CreatePSEmitter(string name, Transform transform)
    {
        ParticlesEmitter pse = Instantiate<ParticlesEmitter>(StringToPSEmitter(name), this.transform);
        Vector2 pos = (transform as RectTransform).anchoredPosition;
        (pse.transform as RectTransform).anchoredPosition = pos;
    }

    ParticlesEmitter StringToPSEmitter(string name)
    {
        switch (name)
        {
            case collision:
                return collisionPSPrefab;
        }

        Debug.Log("Can not find any PS emitter with name: " + name);
        return null;
    }
}
