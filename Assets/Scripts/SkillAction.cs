﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillAction : MonoBehaviour
{
    public bool isPlayerAction;
    public int skillIdx = 0;

    public UITouchHandler touchHnd;

    // Start is called before the first frame update
    void Start()
    {
        touchHnd.LongPressTouch += LongPress;
        touchHnd.EndTouch += EndPress;
    }
    
    void LongPress()
    {
        if (isPlayerAction)
            GamePlay_Demo2.Instance.ShowPlayerSkillHint(skillIdx);
        else
            GamePlay_Demo2.Instance.ShowEnemySkillHint(skillIdx);
    }

    void EndPress(Vector2 pos)
    {
        if (isPlayerAction)
            GamePlay_Demo2.Instance.HidePlayerSkillHint();
        else
            GamePlay_Demo2.Instance.HideEnemySkillHint();
    }
}
