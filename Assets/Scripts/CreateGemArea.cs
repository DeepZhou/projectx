﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateGemArea : MonoBehaviour
{
    public UITouchHandler touchHandler;
    public Image background;

    void Start()
    {
        touchHandler.BeginTouch += OnBeginTouch;
        //ShowEffect();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    public void ShowEffect()
    {
        // StartCoroutine(PlayEffectAnim());

        AsyncFuncHandler.DoAsyncFunc(12f, PlayEffectAnim);
    }

    void OnBeginTouch(Vector2 pos)
    {
        StopAllCoroutines();
        gameObject.SetActive(false);
        pos.x -= Screen.width * 0.5f;
        GamePlay_Demo2.Instance.CreateNewGem(pos.x);
        GamePlay_Demo2.Instance.SetStatusToAiming();        
    }

    void PlayEffectAnim(float deltaTime)
    {
        deltaTime = deltaTime * 12f;
        int count = Mathf.FloorToInt(deltaTime);
        bool fadeIn = count % 2 == 0;
        Color c = background.color;
        if (fadeIn)
            c.a = Mathf.Lerp(0f, 1f, deltaTime - (float)count);
        else
            c.a = Mathf.Lerp(1f, 0f, deltaTime - (float)count);
        background.color = c;
    }

    IEnumerator PlayEffectAnim()
    {
        float deltaTime = 0f;
        float dura = 2f;
        Color c = background.color;
        bool fadeIn = true;
        float a = 0f;

        while (dura > 0f)
        {
            deltaTime += Time.deltaTime * 2f;
            dura -= Time.deltaTime;

            if (fadeIn)
                a = Mathf.Lerp(0f, 1f, deltaTime);
            else
                a = Mathf.Lerp(1f, 0f, deltaTime);

            c.a = a;
            background.color = c;

            if (deltaTime >= 1f)
            {
                deltaTime = 0f;
                fadeIn = !fadeIn;
            }

            yield return null;
        }
    }
}
