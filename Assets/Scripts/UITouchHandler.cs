﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UITouchHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler,
	IPointerEnterHandler, IPointerExitHandler
{
    ///////////////////////////////////////
    // Setting properties
    ///////////////////////////////////////
    public bool showLog = true;
    public float longPressThresold = 5f;
	public float longPressTime = 1f;
    public bool enableTouch = true;

    ///////////////////////////////////////
    // Touch state enums
    ///////////////////////////////////////
    enum TouchState
    {
        None = 0,
        Begin,
        Moving,
        End,
        Click,
        LongPress
    }

    ///////////////////////////////////////
    // Vars for touch event handle
    ///////////////////////////////////////
    private int mouseIndex;
    private bool startTouchEvent;
    private bool readyToEndTouchEvent;
    protected bool doClick;
	protected bool doLongPress;
    private TouchState ts;
    private Vector2 startPos;
    private Vector2 lastPos;
    private Vector2 nowPos;

    ///////////////////////////////////////
    // Register touch events
    ///////////////////////////////////////
    public delegate void TouchBegin(Vector2 beingPos);
    public delegate void TouchMoving(Vector2 deltaPos);
    public delegate void TouchEnd(Vector2 endPos);
    public delegate void TouchClick();
    public delegate void TouchLongPress();

    public event TouchBegin BeginTouch;
    public event TouchMoving MovingTouch;
    public event TouchEnd EndTouch;
    public event TouchClick ClickTouch;
    public event TouchLongPress LongPressTouch;

    ///////////////////////////////////////
    // Mono behaviour methods and touch handle methods
    ///////////////////////////////////////
    void Start()
    {
        startTouchEvent = false;
        readyToEndTouchEvent = false;
		doClick = false;
		doLongPress = false;

        // Default mouse left
        mouseIndex = 0;
        ts = TouchState.None;

        BeginTouch += BeginTouchEvent;
        MovingTouch += MovingTouchEvent;
        EndTouch += EndTouchEvent;
        ClickTouch += ClickTouchEvent;
        LongPressTouch += LongPressTouchEvent;
    }

    // Checking self touch start rely on Unity's IPointerDown
    public void OnPointerDown(PointerEventData eventData)
    {
        startTouchEvent = true;
    }

    // Checking self touch end rely on Unity's IPointerUp
    public void OnPointerUp(PointerEventData eventData)
    {
        readyToEndTouchEvent = true;
    }

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (ts == TouchState.Begin || ts == TouchState.Moving)
			doClick = true;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		if (ts == TouchState.Begin || ts == TouchState.Moving)
			doClick = false;
	}

    void OnDestroy()
    {
        BeginTouch -= BeginTouchEvent;
        MovingTouch -= MovingTouchEvent;
        EndTouch -= EndTouchEvent;
        ClickTouch -= ClickTouchEvent;
        LongPressTouch -= LongPressTouchEvent;
    }

    void Update()
    {
        // 依賴Unity pointer相關事件去確認是否執行自身的touch事件,
        // 否則只要有觸碰則所有掛此腳本的物件都會觸發touch事件
        if (!startTouchEvent)
            return;

        if (enableTouch)
        {
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(mouseIndex))
            {
                BeginTouchHandle(Input.mousePosition);
            }
            else if (Input.GetMouseButton(mouseIndex))
            {
				MovingTouchHandle(Input.mousePosition);
            }
            else if (Input.GetMouseButtonUp(mouseIndex))
            {
                EndTouchHandle(Input.mousePosition);
            }
#elif UNITY_ANDROID || UNITY_IOS
            if (Input.touchCount > 0)
            {
                TouchPhase phase = Input.GetTouch(0).phase;

                if (phase == TouchPhase.Began)
                {
                    BeginTouchHandle(Input.GetTouch(0).position);
                }
                else if (phase == TouchPhase.Moved)
                {
					MovingTouchHandle(Input.GetTouch(0).position);
                }
                else if (phase == TouchPhase.Ended || phase == TouchPhase.Canceled)
                {
					EndTouchHandle(Input.GetTouch(0).position);
                }
            }
#endif
        }

        // 用來等最後的事件結束後判斷Touch事件是否結束
        if (readyToEndTouchEvent)
        {
            startTouchEvent = false;
            readyToEndTouchEvent = false;
        }
    }

	void BeginTouchHandle(Vector2 pos)
	{
		startPos = lastPos = nowPos = pos;
		BeginTouch?.Invoke(nowPos);
		StartCoroutine(CheckClickedOrLongPressed());
	}

	void MovingTouchHandle(Vector2 pos)
	{
		nowPos = pos;
		float dist = (nowPos - lastPos).magnitude;
        MovingTouch?.Invoke(nowPos - lastPos);
        CheckLongPress(pos);
		lastPos = nowPos;
	}

	void EndTouchHandle(Vector2 pos)
	{
		nowPos = pos;
		
		if (doClick == true)
		{
			doClick = false;
			doLongPress = false;
			ClickTouch?.Invoke();
		}
		EndTouch?.Invoke(nowPos);
		StopCoroutine(CheckClickedOrLongPressed());
	}

	void CheckLongPress(Vector2 pos)
	{
		float dist = (pos - startPos).magnitude;
		if (dist > longPressThresold)
		{
			doLongPress = false;
		}
	}

	IEnumerator CheckClickedOrLongPressed()
    {
		doClick = true;
		doLongPress = true;
		
        yield return new WaitForSeconds(longPressTime);

		if (doLongPress == true)
		{
			doClick = false;
			doLongPress = false;
			LongPressTouch?.Invoke();
		}
    }

    ///////////////////////////////////////
    // Touch state handle
    ///////////////////////////////////////
    void BeginTouchEvent(Vector2 beginPos)
    {
		ChangeTouchState(TouchState.Begin);
    }

    void MovingTouchEvent(Vector2 deltaPos)
    {
		ChangeTouchState(TouchState.Moving);
    }

    void EndTouchEvent(Vector2 endPos)
    {
		ChangeTouchState(TouchState.End);
    }

    void ClickTouchEvent()
    {
		ChangeTouchState(TouchState.Click);
    }

    void LongPressTouchEvent()
    {
		ChangeTouchState(TouchState.LongPress);
    }

    ///////////////////////////////////////
    // Custom methods
    ///////////////////////////////////////
	void ChangeTouchState(TouchState nowTS)
	{
		if (ts != nowTS)
		{
			Log("Now touch state: " + nowTS);
			ts = nowTS;
		}
	}
    protected void Log(object msg)
    {
        if (showLog)
            Debug.Log(msg);
    }
}
