﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ActionItem : MonoBehaviour, IPointerDownHandler
{
    public Sprite[] actionSprites;

    public enum ActionType
    {
        FastAttack,
        HeavyAttack,
        Dodge,
        Defence
    }
    public ActionType type;

    public Image image;

    UIDragHandler dragHandler;
    GameObject touchedGO;
    ActionContainer lastSetContainer;

    public void OnPointerDown(PointerEventData data)
    {
        // Debug.Log("HIHI");
    }

    public void Setup(ActionType type, Sprite sprite = null)
    {
        this.type = type;
        image.sprite = actionSprites[(int)this.type];

        if (sprite != null)
            image.sprite = sprite;
    }

    void Start()
    {
        dragHandler = this.GetComponent<UIDragHandler>();
        if (dragHandler != null)
            dragHandler.EndTouch += OnDragEnd;
    }

    private void OnDestroy()
    {
        if (dragHandler != null)
            dragHandler.EndTouch += OnDragEnd;
    }

    void OnDragEnd(Vector2 pos)
    {
        if (touchedGO != null)
        {
            if (lastSetContainer != null)
            {
                if (lastSetContainer != touchedGO.GetComponent<ActionContainer>())
                    lastSetContainer.SetActionItem(null);

                lastSetContainer = touchedGO.GetComponent<ActionContainer>();
                lastSetContainer.SetActionItem(this);
            }
        }
        else
        {
            if (lastSetContainer != null)
            {
                lastSetContainer.SetActionItem(null);
                lastSetContainer = null;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D obj)
    {
        touchedGO = obj.gameObject;
    }

    void OnTriggerExit2D(Collider2D obj)
    {
        if (touchedGO == obj.gameObject)
            touchedGO = null;
    }

    // void OnTriggerStay2D(Collider2D obj)
    // {
    //     Debug.Log(obj.name);
    // }
}
