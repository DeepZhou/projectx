﻿using System;
using UnityEngine;
using UnityEngine.UI;
using DStudio.Utilities;

public class SwitchStageScreen : MonoBehaviour
{
    public static SwitchStageScreen Instance = null;

    public RawImage background;

    void Awake()
    {
        Instance = this;
    }

    public void FadeIn(float delay, float duration, Action comepleteAction = null)
    {
        StartCoroutine(GeneralUtil.LerpAnimation(delay, duration, 
        (prog) =>
        {
            Color c = background.color;
            c.a = Mathf.Lerp(1f, 0f, prog);
            background.color = c;
        },
        comepleteAction));
    }

    public void FadeOut(float delay, float duration, Action comepleteAction = null)
    {
        StartCoroutine(GeneralUtil.LerpAnimation(delay, duration, 
        (prog) =>
        {
            Color c = background.color;
            c.a = Mathf.Lerp(0f, 1f, prog);
            background.color = c;
        },
        comepleteAction));
    }
}
