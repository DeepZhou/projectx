﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestUITouch : MonoBehaviour
{
    public UITouchHandler touchHandler;

    public Image target;

    // Start is called before the first frame update
    void Start()
    {
        touchHandler = GetComponent<UITouchHandler>();

        if (target != null)
        {
            touchHandler.BeginTouch += OnBeginTouch;
            touchHandler.MovingTouch += OnMovingTouch;
            touchHandler.EndTouch += OnEndTouch;
            touchHandler.ClickTouch += OnClick;
            touchHandler.LongPressTouch += OnLongPressed;
        }
    }

    void OnBeginTouch(Vector2 pos)
    {
        Debug.Log("Begin touch!");
    }

    void OnMovingTouch(Vector2 pos)
    {
        Debug.Log("Moving touch!");
    }

    void OnEndTouch(Vector2 pos)
    {
        Debug.Log("End touch!");
    }

    void OnClick()
    {
        StartCoroutine(FadeInOutAnime(0f, 0f, false, () =>
        {
            StartCoroutine(FadeInOutAnime(0f, 0f, true));
        }));
    }

    void OnLongPressed()
    {
        StartCoroutine(FadeInOutAnime(1f, 1f, false, () =>
        {
            StartCoroutine(FadeInOutAnime(1f, 0f, true));
        }));
    }

    IEnumerator FadeInOutAnime(float startDelay, float endDelay, bool isFadeIn, System.Action finishCallback = null)
    {
        yield return new WaitForSeconds(startDelay);

        float startA = isFadeIn ? 0f : 1f;
        float endA = 1f - startA;
        float dt = 0f;
        Color c = Color.white;

        while (dt <= 1f)
        {
            dt += Time.deltaTime;
            float a = Mathf.Lerp(startA, endA, dt);
            c = target.color;
            c.a = a;
            target.color = c;
            yield return null;
        }

        c = target.color;
        c.a = endA;
        target.color = c;

        yield return new WaitForSeconds(endDelay);

        finishCallback?.Invoke();
    }
}
