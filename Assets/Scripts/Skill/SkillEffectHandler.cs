﻿using UnityEngine;
using UnityEngine.UI;

public class SkillEffectHandler : MonoBehaviour
{
    [Range(0f, 1f)]
    public float effectProg = 0f;

    public GameObject particleGO;

    float preEffectProg = 0f;
    float skillSprWidth = 0f;
    Material effectMat;

    // Start is called before the first frame update
    void Start()
    {
        if (skillSprWidth == 0f)
        {
            skillSprWidth = (this.transform as RectTransform).sizeDelta.x;
        }

        effectMat = this.GetComponent<Image>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (skillSprWidth == 0 || effectProg == preEffectProg)
            return;

        Vector2 pos = (particleGO.transform as RectTransform).anchoredPosition;
        pos.x = effectProg * skillSprWidth;
        (particleGO.transform as RectTransform).anchoredPosition = pos;

        if (effectProg >= 1f || effectProg <= 0f)
        {
            particleGO.GetComponent<ParticleSystem>().Stop();
        }
        else
        {
            particleGO.GetComponent<ParticleSystem>().Play();
            effectMat.SetFloat("_AlphaOffset", effectProg);
        }

        preEffectProg = effectProg;
    }
}
