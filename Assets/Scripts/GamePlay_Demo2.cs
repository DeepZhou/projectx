﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharStatus
{
    public int MaxHP;
    public int NowHP;
    public int MaxMP;
    public int NowMP;

    public void AddHP(int val)
    {
        NowHP += val;

        if (NowHP > MaxHP)
            NowHP = MaxHP;
        else if (NowHP < 0)
            NowHP = 0;
    }

    public void AddMP(int val)
    {
        NowMP += val;

        if (NowMP > MaxMP)
            NowMP = MaxMP;
        else if (NowMP < 0)
            NowMP = 0;
    }
}

public class GamePlay_Demo2 : MonoBehaviour
{
    public enum GameStatus
    {
        PutGem,
        Aiming,
        PlayerAttack,
        EnemyAttack,
    }
    public GameStatus NowGameStatus = GameStatus.PutGem;

    public static GamePlay_Demo2 Instance = null;

    public GameObject NowSelectedSoul = null;
    public GameObject nowEnemySelectedSoul = null;

    public CharStatusHandler playerStatusHnd;
    public CharStatusHandler enemyStatusHnd;

    public SkillHint playerSkillHint;
    public SkillHint enemySkillHint;

    // Stage item refs
    public Transform stageTransform;
    public Sprite[] stageObjectSprites;
    public Sprite[] gameItemSprites;
    public Sprite[] gemSprites;
    public GameObject stageItemPrefab;
    public List<GameObject> stageObjectList;
    public List<GameObject> gameItemList;

    public CreateGemArea createGemArea;

    public GameObject gemPrefab;
    public Transform gemsTransform;

    CharStatus playerStatus;
    CharStatus enemyStatus;

    const int playerMaxHP = 200;
    const int playerMaxMP = 100;
    const int enemyMaxHP = 200;
    const int enemyMaxMP = 100;

    List<int> playerSkillIdxList;
    List<int> enemySkillIdxList;

    JSONObject skillJson;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    IEnumerator Start()
    {
        Init();
        LoadSkill();

        yield return new WaitForSeconds(1f);

        SetStatusToPutGem();
        CreateStageObjects();
        CreateGameItems();
    }

    void Init()
    {
        playerStatus = new CharStatus { MaxHP = playerMaxHP, NowHP = 100, MaxMP = playerMaxMP, NowMP = 0 };
        enemyStatus = new CharStatus { MaxHP = enemyMaxHP, NowHP = 100, MaxMP = enemyMaxMP, NowMP = 0 };
        AddPlayerHP(100);
        AddPlayerMP(-100);
        AddEnemyHP(100);
        AddEnemyMP(-100);
    }

    void LoadSkill()
    {
        if (skillJson == null)
            skillJson = new JSONObject(Resources.Load<TextAsset>("skill").text);

        Debug.Log(skillJson.Print());

        int[] idxArr = new int[8];
        for (int i = 0; i < 8; i++)
        {
            idxArr[i] = i;
        }

        for (int i = 0; i < 8; i++)
        {
            int rand = Random.Range(i, 8);
            int tmp = idxArr[rand];
            idxArr[rand] = idxArr[i];
            idxArr[i] = tmp;
        }

        if (playerSkillIdxList == null)
            playerSkillIdxList = new List<int>();
        else
            playerSkillIdxList.Clear();

        if (enemySkillIdxList == null)
            enemySkillIdxList = new List<int>();
        else
            enemySkillIdxList.Clear();

        for (int i = 0; i < 4; i++)
            playerSkillIdxList.Add(idxArr[i]);

        for (int i = 4; i < 8; i++)
            enemySkillIdxList.Add(idxArr[i]);
    }

    public void SetStatusToPutGem()
    {
        NowGameStatus = GameStatus.PutGem;
        Debug.Log("Now game status: " + NowGameStatus.ToString());
        createGemArea.gameObject.SetActive(true);
        createGemArea.ShowEffect();
    }

    public void SetStatusToAiming()
    {
        NowGameStatus = GameStatus.Aiming;
        Debug.Log("Now game status: " + NowGameStatus.ToString());
    }

    public void SetStatusToPlayerAttack()
    {
        NowGameStatus = GameStatus.PlayerAttack;
        Debug.Log("Now game status: " + NowGameStatus.ToString());
    }

    public void SetStatusToEnemyAttack()
    {
        NowGameStatus = GameStatus.EnemyAttack;
        Debug.Log("Now game status: " + NowGameStatus.ToString());
        StartCoroutine(WaitForEnemyActionEnd());
    }

    IEnumerator WaitForEnemyActionEnd()
    {
        yield return new WaitForSeconds(1f);
        SetStatusToPutGem();
    }

    public void CreateNewGem(float posX)
    {
        if (NowGameStatus != GameStatus.PutGem)
            return;

        GameObject gem = GameObject.Instantiate(gemPrefab, gemsTransform);
        Vector2 pos = gem.transform.GetRectTransform().anchoredPosition;
        pos.x = posX;
        gem.transform.GetRectTransform().anchoredPosition = pos;
        SetStatusToAiming();
    }

    public void AddPlayerHP(int val)
    {
        playerStatus.AddHP(val);
        playerStatusHnd.AddHPBarProgress((float)val / (float)playerStatus.MaxHP);
    }

    public void AddPlayerMP(int val)
    {
        playerStatus.AddMP(val);
        playerStatusHnd.AddMPBarProgress((float)val / (float)playerStatus.MaxMP);
    }

    public void AddEnemyHP(int val)
    {
        enemyStatus.AddHP(val);
        enemyStatusHnd.AddHPBarProgress((float)val / (float)enemyStatus.MaxHP);
    }

    public void AddEnemyMP(int val)
    {
        enemyStatus.AddMP(val);
        enemyStatusHnd.AddMPBarProgress((float)val / (float)enemyStatus.MaxMP);
    }

    public void ShowPlayerSkillHint(int idx)
    {
        string name = skillJson[idx].GetField("name").str;
        int damage = (int)skillJson[idx].GetField("damage").i;
        int cost = (int)skillJson[idx].GetField("cost").i;
        string action = skillJson[idx].GetField("action").str;
        playerSkillHint.Setup(name, damage, cost, action);
        playerSkillHint.gameObject.SetActive(true);
    }

    public void HidePlayerSkillHint()
    {
        playerSkillHint.gameObject.SetActive(false);
    }

    public void ShowEnemySkillHint(int idx)
    {
        string name = skillJson[idx].GetField("name").str;
        int damage = (int)skillJson[idx].GetField("damage").i;
        int cost = (int)skillJson[idx].GetField("cost").i;
        string action = skillJson[idx].GetField("action").str;
        enemySkillHint.Setup(name, damage, cost, action);
        enemySkillHint.gameObject.SetActive(true);
    }

    public void HideEnemySkillHint()
    {
        enemySkillHint.gameObject.SetActive(false);
    }

    public void CreateStageObjects(bool destroyExist = true)
    {
        if (stageObjectList == null)
            stageObjectList = new List<GameObject>();
        else
        {
            if (destroyExist)
            {
                foreach (GameObject item in stageObjectList)
                    Destroy(item);
                stageObjectList.Clear();
            }
        }

        int stageObjNum = 6;

        for (int i = 0; i < stageObjNum; i++)
        {
            GameObject stageObj = Instantiate(stageItemPrefab, stageTransform);
            SetSceneObjectType(ref stageObj);
            (stageObj.transform as RectTransform).anchoredPosition = new Vector2(Random.Range(-400f, 400f), Random.Range(-400f, 400f));
            stageObjectList.Add(stageObj);
        }
    }

    public void CreateGameItems(bool destroyExist = true)
    {
        if (gameItemList == null)
            gameItemList = new List<GameObject>();
        else
        {
            if (destroyExist)
            {
                foreach (GameObject item in gameItemList)
                    Destroy(item);
                gameItemList.Clear();
            }
        }

        int gameItemNum = 6;

        for (int i = 0; i < gameItemNum; i++)
        {
            GameObject gameItem = Instantiate(stageItemPrefab, stageTransform);
            SetGameItemType(ref gameItem);
            (gameItem.transform as RectTransform).anchoredPosition = new Vector2(Random.Range(-400f, 400f), Random.Range(-400f, 400f));
            gameItemList.Add(gameItem);
        }
    }

    void SetSceneObjectType(ref GameObject obj, int idx = -1)
    {
        BaseCollision col = obj.GetComponent<BaseCollision>();
        Image img = obj.GetComponent<Image>();

        if (col == null)
        {
            Debug.Log("Cannot find BaseCollision on this gameobject, name: " + obj.name);
            return;
        }

        if (img == null)
        {
            Debug.Log("Cannot find Image on this gameobject, name: " + obj.name);
            return;
        }

        int _idx = idx;
        if (_idx <= -1)
        {
            _idx = Random.Range(0, 5);
        }

        switch (_idx)
        {
            case 0:
                col.objectType = SceneObjectType.Wall;
                break;
            case 1:
                col.objectType = SceneObjectType.HugeRock;
                break;
            case 2:
                col.objectType = SceneObjectType.BrokenRock;
                break;
            case 3:
                col.objectType = SceneObjectType.BuffArea;
                obj.GetComponent<BoxCollider2D>().isTrigger = true;
                break;
            case 4:
                col.objectType = SceneObjectType.DebuffArea;
                obj.GetComponent<BoxCollider2D>().isTrigger = true;
                break;
        }
        img.sprite = stageObjectSprites[_idx];
    }

    void SetGameItemType(ref GameObject obj, int idx = -1)
    {
        BaseCollision col = obj.GetComponent<BaseCollision>();
        Image img = obj.GetComponent<Image>();
        obj.GetComponent<BoxCollider2D>().isTrigger = true;

        if (col == null)
        {
            Debug.Log("Cannot find BaseCollision on this gameobject, name: " + obj.name);
            return;
        }

        if (img == null)
        {
            Debug.Log("Cannot find Image on this gameobject, name: " + obj.name);
            return;
        }

        int _idx = idx;
        if (_idx <= -1)
        {
            _idx = Random.Range(0, 6);
        }

        switch (_idx)
        {
            case 0:
                col.objectType = SceneObjectType.GameItem;
                col.gameItemType = GameItemType.FistToken;
                break;
            case 1:
                col.objectType = SceneObjectType.GameItem;
                col.gameItemType = GameItemType.LegToken;
                break;
            case 2:
                col.objectType = SceneObjectType.GameItem;
                col.gameItemType = GameItemType.HumanToken;
                break;
            case 3:
                col.objectType = SceneObjectType.GameItem;
                col.gameItemType = GameItemType.SwordToken;
                break;
            case 4:
                col.objectType = SceneObjectType.GameItem;
                col.gameItemType = GameItemType.Bun;
                break;
            case 5:
                col.objectType = SceneObjectType.GameItem;
                col.gameItemType = GameItemType.Pill;
                break;
        }
        img.sprite = gameItemSprites[_idx];
    }
}
