﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIAnimation : MonoBehaviour {
	public float origWidth;

	public Image HpBar;

	// Use this for initialization
	void Start () {
		origWidth = HpBar.GetComponent<RectTransform>().rect.width;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void UpdateHpBarWidth(float newRatio)
	{
		float origH = HpBar.GetComponent<RectTransform>().rect.height;
		HpBar.GetComponent<RectTransform>().sizeDelta = new Vector2(origWidth*newRatio, origH);
	}

	public void DoAnim()
	{
		StartCoroutine( IEDoAnim(0.2f, 0.8f, 30) );
	}

	private void StartAnim()
	{

	}

	private IEnumerator IEDoAnim(float origRatio, float destRatio, float duration)
	{
		float _orig = origRatio;

		while (_orig < destRatio)
		{
			yield return null;
			_orig+=0.03f;
			UpdateHpBarWidth(_orig);
		}

		yield return null;
	}
}
