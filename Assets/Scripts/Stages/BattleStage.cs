﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DStudio.Utilities;

public class BattleStage : GameStageBasic
{
    public SkillEffectHandler skillEffect;

    override public void Init()
    {
        base.Init();

        StartCoroutine(GeneralUtil.LerpAnimation(2f, 2f,
        (prog) =>
        {
            skillEffect.effectProg = prog;
        }));
    }
}
