﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStageBasic : MonoBehaviour
{
    public virtual void Init()
    {
        Debug.Log(this.name + " Init()");
    }

    public virtual void Deinit()
    {
        Debug.Log(this.name + " Deinit()");
    }

    void OnEnable()
    {
        Init();
    }

    void OnDisable()
    {
        Deinit();
    }

    public void SwitchToNextPage()
    {
        GameStageManager.Instance.TestSwitchToNextPage();
    }
}
