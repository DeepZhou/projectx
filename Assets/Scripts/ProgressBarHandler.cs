﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarHandler : MonoBehaviour
{
    public Image mask;
    public Image value;

    [Range(0f, 1f)]
    public float progress = 1f;
    private float lastProgress;

    private RectTransform maskRectTransform;
    private RectTransform valueRectTransform;

    private float widthMax;

    void Start()
    {
        widthMax = (transform as RectTransform).sizeDelta.x;
        Vector2 size;

        // Mask init
        maskRectTransform = transform.Find("Mask") as RectTransform;
        size = maskRectTransform.sizeDelta;
        size.x = widthMax;
        maskRectTransform.sizeDelta = size;

        // Value init
        valueRectTransform = maskRectTransform.Find("Value") as RectTransform;
        size = valueRectTransform.sizeDelta;
        size.x = widthMax;
        valueRectTransform.sizeDelta = size;

        lastProgress = progress;
        UpdateProgress(progress);
    }

    void Update()
    {
        if (progress != lastProgress &&
            progress <= 1f && progress >= 0f)
        {
            lastProgress = progress;
            UpdateProgress(progress);
        }
    }

    public void UpdateProgress(float progress)
    {
        this.progress = progress;
        float width = widthMax * progress;
        Vector2 size = maskRectTransform.sizeDelta;
        size.x = width;
        maskRectTransform.sizeDelta = size;
    }
}
