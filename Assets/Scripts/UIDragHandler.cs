﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDragHandler : UITouchHandler
{
    public bool enableObjectDragging;
    public bool enableThrowing;
    public float throwSpeedMultipler = 1f;
    public float weight = 1f;

    RectTransform rectTransform;

    float duration = 1f;
    Vector2 nowPos;
    Vector2 lastPos;
    Vector2 nowSpd = Vector2.zero;

    Rigidbody2D rigidbody;

    ///////////////////////////////////////
    // Register touch events
    ///////////////////////////////////////
    public delegate void DragBegin(Vector2 beingPos);
    public delegate void DragMoving(Vector2 deltaPos);
    public delegate void DragEnd(Vector2 endPos);

    public event DragBegin BeginDrag;
    public event DragMoving MovingDrag;
    public event DragEnd EndDrag;

    void Start()
    {
        rectTransform = this.transform as RectTransform;
        rigidbody = this.GetComponent<Rigidbody2D>();

        this.BeginTouch += OnBeginDrag;
        this.MovingTouch += OnDrag;
        this.EndTouch += OnEndDrag;
    }

    // void LateUpdate()
    // {
    //     if (enableThrowing)
    //     {
    //         if (duration <= 1f && nowSpd.magnitude > 0f)
    //         {
    //             if (weight <= 0f)
    //                 duration = 0;
    //             else
    //                 duration += Time.deltaTime / weight;
                
    //             Vector2 pos = rectTransform.anchoredPosition;
    //             pos += nowSpd * (1 - easeOutQuad(duration)) * throwSpeedMultipler;
    //             rectTransform.anchoredPosition = pos;
    //         }
    //     }
    // }

    void OnDestroy()
    {
        this.BeginTouch -= OnBeginDrag;
        this.MovingTouch -= OnDrag;
        this.EndTouch -= OnEndDrag;
    }

    void OnBeginDrag(Vector2 startPos)
    {
        nowSpd = Vector2.zero;
        duration = 0;
        this.nowPos = startPos;
        Vector2 pos = rectTransform.anchoredPosition;
        lastPos = pos;
        BeginDrag?.Invoke(startPos);

        GamePlay_Demo2.Instance.NowSelectedSoul = this.gameObject;
    }

    void OnDrag(Vector2 deltaPos)
    {
        Vector2 pos = rectTransform.anchoredPosition;
        lastPos = pos;
        pos += deltaPos;

        if (enableObjectDragging)
            rectTransform.anchoredPosition = pos;

        if (deltaPos.magnitude > 0f)
            this.doClick = false;
        MovingDrag?.Invoke(deltaPos);
    }

    void OnEndDrag(Vector2 endPos)
    {
        // nowSpd = (rectTransform.anchoredPosition - lastPos) / Time.deltaTime;
        // nowSpd = endPos - this.nowPos;
        // SetSpd(nowSpd);
        
        // if (rigidbody != null)
        // {
        //     rigidbody.AddForce(nowSpd);
        //     Debug.Log("Spd: " + nowSpd);
        // }

        EndDrag?.Invoke(endPos);
    }

    float easeOutQuad(float x)
    {
        return 1 - (1 - x) * (1 - x);
    }

    public void SetSpd(Vector2 spdVector)
    {
        if (!enableThrowing)
            Log("Throwing disabled, this function won't working");

        duration = 0f;
        nowSpd = spdVector;

        if (rigidbody != null)
        {
            rigidbody.AddForce(nowSpd);
            Debug.Log("Spd: " + nowSpd);
        }
    }

    public Vector2 GetSpd()
    {
        return nowSpd;
    }
}
