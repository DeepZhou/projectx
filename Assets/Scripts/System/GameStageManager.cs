﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStageManager : MonoBehaviour
{
    public static GameStageManager Instance = null;

    public Transform gameStageRoot;
    GameObject[] gameStageGOs;

    GD.GameStage nowShowingStage = GD.GameStage.None;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        InitGameStageGORefs();
        SwitchDefaultStage();
    }

    void InitGameStageGORefs()
    {
        gameStageGOs = new GameObject[gameStageRoot.childCount];

        for (int i = 0; i < gameStageRoot.childCount; i++)
        {
            gameStageGOs[i] = gameStageRoot.GetChild(i).gameObject;
            gameStageGOs[i].SetActive(false);
        }
    }

    void SwitchDefaultStage()
    {
        SwitchStage(GD.GameStage.GameTitle);
    }

    public void SwitchStage(GD.GameStage stage)
    {
        if (nowShowingStage == stage) return;

        SwitchStageScreen.Instance.FadeOut(0f, 1f,
        () =>
        {
            // Fade out complete action
            if (nowShowingStage != GD.GameStage.None)
                gameStageGOs[(int)nowShowingStage].SetActive(false);
            gameStageGOs[(int)stage].SetActive(true);
            nowShowingStage = stage;
        });
        SwitchStageScreen.Instance.FadeIn(1.5f, 1f);
    }

    public void TestSwitchToNextPage()
    {
        var stage = nowShowingStage;

        if (stage == GD.GameStage.BattleStage)
            stage = GD.GameStage.None;

        stage++;

        SwitchStage(stage);
    }
}
