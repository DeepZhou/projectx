﻿public class GD // Global Define
{
    // 遊戲舞台, 同時只能有一個舞台開啟
    public enum GameStage
    {
        None = -1,
        GameTitle, // 開頭畫面
        HomeMenu, // 自家主畫面, 養成階段的每個回合開始的畫面
        MapMenu, // 大地圖, 外出冒險的開始畫面
        BattleStage, // 戰鬥舞台, 觸發戰鬥時的畫面
        Calendar, // 日曆
    }
}
