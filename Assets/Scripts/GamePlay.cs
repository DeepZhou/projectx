﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlay : MonoBehaviour
{
    public ActionItem actionItemPrefab;

    public ActionContainer[] preparedContainers;
    public Transform gemRoot;

    void ArrangeActionItem()
    {
        int idx = 0;
        while (ArrangeOneActionItem(idx)){ idx++; }
    }

    bool ArrangeOneActionItem(int startIdx)
    {
        bool arrangeWorking = false;

        int emptyContainerIdx = -1;

        for (int i = startIdx; i < preparedContainers.Length; i++)
        {
            if (preparedContainers[i].GetActionItem() == null)
            {
                emptyContainerIdx = i;
                break;
            }
        }

        int arrangeContainIdx = -1;
        for (int i = emptyContainerIdx + 1; i < preparedContainers.Length; i++)
        {
            if (preparedContainers[i].GetActionItem() != null)
            {
                arrangeContainIdx = i;
                break;
            }
        }

        if (emptyContainerIdx != -1 && arrangeContainIdx != -1)
        {
            preparedContainers[emptyContainerIdx].SetActionItem(preparedContainers[arrangeContainIdx].GetActionItem());
            preparedContainers[arrangeContainIdx].SetActionItem(null);

            arrangeWorking = true;
        }

        return arrangeWorking;
    }

    void GenerateActionItem()
    {
        for (int i = 0; i < preparedContainers.Length; i++)
        {
            if (preparedContainers[i].GetActionItem() == null)
            {
                ActionItem item = Instantiate<ActionItem>(actionItemPrefab, gemRoot, false);
                int random = Random.Range(0, 4);
                item.Setup((ActionItem.ActionType)random);
                preparedContainers[i].SetActionItem(item);
            }
        }
    }

    public void GenerateActionGemButtonPressed()
    {
        ArrangeActionItem();
        GenerateActionItem();
    }
}
