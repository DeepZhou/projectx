﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintArrowHandler : MonoBehaviour
{
    public Image arrow;

    public float spdMultipler = 1f;

    UIDragHandler dragHnd;

    Vector2 beginPos;
    Vector2 nowPos;
    Vector2 origin = new Vector2(-1f, 0f);
    Vector2 arrowOriginSize;

    private void Start() {
        dragHnd = gameObject.GetComponent<UIDragHandler>();

        arrowOriginSize = arrow.rectTransform.sizeDelta;

        dragHnd.BeginDrag += OnBeginDrag;
        dragHnd.MovingDrag += OnDrag;
        dragHnd.EndDrag += OnEndDrag;
    }

    public void OnBeginDrag(Vector2 beginPos)
    {
        this.beginPos = beginPos;
        this.nowPos = beginPos;
        resizeArrow((nowPos - beginPos).magnitude);
        arrow.gameObject.SetActive(true);
    }

    public void OnDrag(Vector2 deltaPos)
    {
        nowPos += deltaPos;
        Vector2 v2 = (nowPos - beginPos) - origin;
        float angle = angle = Mathf.Atan2(v2.y, v2.x)*Mathf.Rad2Deg;
        arrow.transform.eulerAngles = new Vector3(0f, 0f, angle + 180f);

        resizeArrow((nowPos - beginPos).magnitude);
    }

    public void OnEndDrag(Vector2 endPos)
    {
        if (GamePlay_Demo2.Instance.NowGameStatus != GamePlay_Demo2.GameStatus.Aiming)
            return;

        arrow.gameObject.SetActive(false);
        dragHnd.SetSpd((endPos - this.beginPos) * spdMultipler);
        GamePlay_Demo2.Instance.SetStatusToPlayerAttack();
    }

    void resizeArrow(float dragDistance)
    {
        float sizeXScale = dragDistance / arrowOriginSize.x;
        float sizeYScale = arrowOriginSize.x / dragDistance;

        if (sizeYScale > 1f)
            sizeYScale = 1f;

        arrow.rectTransform.sizeDelta = new Vector2(arrowOriginSize.x * sizeXScale, arrowOriginSize.y * sizeYScale);
    }
}
