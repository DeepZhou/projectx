﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public enum AlignDirection {
    Up_2_Down,
    Left_2_Right,
    Down_2_Up,
    Right_2_Left
}

[System.Serializable]
public class ButtonData {
    public string displayText;
    public Button.ButtonClickedEvent clickEvent;
}

public class ButtonPanelManager : MonoBehaviour {

    public Button buttonPrefab;

    public AlignDirection alignDir;
    public float margin;
    public ButtonData[] buttonDataArr;

	// Use this for initialization
	void Start () {
        GenerateButtons();
	}

    void GenerateButtons()
    {
        float panelWidth = ((RectTransform)this.transform).rect.width;
        float panelHeight = ((RectTransform)this.transform).rect.height;
        int itemNum = buttonDataArr.Length;
        float itemSize = 0f;

        switch (alignDir)
        {
            case AlignDirection.Up_2_Down:
            case AlignDirection.Down_2_Up:
                itemSize = (panelHeight - margin * (itemNum + 1)) / itemNum;
                break;
            case AlignDirection.Left_2_Right:
            case AlignDirection.Right_2_Left:
                itemSize = (panelWidth - margin * (itemNum + 1)) / itemNum;
                break;
        }

        int textSize = Mathf.RoundToInt(itemSize * 0.3f);

        Button btn = null;
        RectTransform rectTF = null;
        Text text = null;

        for (int i = 0; i < buttonDataArr.Length; i++)
        {   
            btn = GameObject.Instantiate<Button>(buttonPrefab);
            btn.transform.SetParent(this.transform, false);
            rectTF = ((RectTransform)btn.transform);

            switch (alignDir)
            {
                case AlignDirection.Up_2_Down:
                    rectTF.offsetMin = new Vector3(margin, panelHeight - (itemSize + margin) * (i + 1));
                    rectTF.offsetMax = new Vector3(-margin, -margin - (itemSize + margin) * i);
                    break;
                case AlignDirection.Down_2_Up:
                    rectTF.offsetMin = new Vector3(margin, panelHeight - (itemSize + margin) * (buttonDataArr.Length - i));
                    rectTF.offsetMax = new Vector3(-margin, -margin - (itemSize + margin) * (buttonDataArr.Length - i - 1));
                    break;
                case AlignDirection.Left_2_Right:
                    rectTF.offsetMin = new Vector3(panelWidth - (itemSize + margin) * (buttonDataArr.Length - i), margin);
                    rectTF.offsetMax = new Vector3(-margin - (itemSize + margin) * (buttonDataArr.Length - i - 1), -margin);
                    break;
                case AlignDirection.Right_2_Left:
                    rectTF.offsetMin = new Vector3(panelWidth - (itemSize + margin) * (i + 1), margin);
                    rectTF.offsetMax = new Vector3(-margin - (itemSize + margin) * i, -margin);
                    break;
            }

            text = btn.GetComponentInChildren<Text>();

            if (text != null)
            {
                text.text = buttonDataArr[i].displayText;
                text.fontSize = textSize;
            }

            btn.onClick = buttonDataArr[i].clickEvent;
            btn.gameObject.SetActive(true);

        }
    }
	
    public void ButtonClick()
    {
        Debug.Log("click!");
    }
}
