- 20/05/04
	功能簡介:
		1.將 AutoButtonPanel 拖進主要的canvas

		2.調整panel的大小位置, 接著設定 ButtonPanelManager 參數:
			Button Prefab: 已有預設產生的按鈕prefab, 如果需要改變按鈕的樣子可以再覆蓋掉
			Align Dir: 按鈕的排列軸向, 共有四個方向供選擇
			Margin: 按鈕與四周邊界的距離, 目前按鈕之間的間距也是套用該值
			Button Data Arr: 設定按鈕資料的陣列, 目前有按鈕顯示文字及點擊事件可以設定

		3.按鈕的顯示會根據panel的大小, 設定的按鈕數量, 最後加上margin去平均分配位置