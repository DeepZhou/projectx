﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DStudio.Animator {
    
    public class AnimBase : MonoBehaviour {

        protected bool startPlayAnim;       // 判斷是否開始動畫

        protected bool animPlaying;         // 動畫播放中
        public bool AnimPlaying {
            get
            {
                return animPlaying;
            }
        }

        protected bool animPaused;          // 動畫暫停
        public bool AnimPaused {
            get
            {
                return animPaused;
            }
        }

        protected bool animFinished;        // 動畫結束
        public bool AnimFinished {
            get
            {
                return animFinished;
            }
        }

        protected float deltaTime;          // 動畫開始後的計算時間(累積)
        protected float deltaVal;           // 根據設定的動畫曲線, 帶入目前的計算時間, 得到的一個變量(動畫的結果都是根據這個值去變化)
        protected float delayDeltaTime;     // 動畫開始延遲的計算時間(累積)

        protected float _delay;             // 該次動畫的暫存數值
        protected float _speed;             // 該次動畫的暫存數值
        protected float _duration;          // 該次動畫的暫存數值

        public GameObject targetObj;        // 動畫作用的物件, 若為空值則預設取母物件
        public bool playOnAwake;            // 是否在Start時就自動播放動畫
        public bool playPingPong;           // 是否在結束時倒播回一開始的狀態(兩倍播放時間)
        public bool playReverse;
        public float delay;                 // 動畫開始延遲
        public float speed;                 // 動畫速度(暫無作用)
        public float duration;              // 動畫表演時間(不含延遲)
        public AnimationCurve easingCurve;  // 動畫曲線, 可以設定整段動畫的呈現方式

        protected virtual void Awake() {
            InitProps();
        }

        public void Play(){
            InitProps();
            startPlayAnim = true;
            animPaused = false;
        }

        public void Pause(){
            animPlaying = false;
            animPaused = true;
        }

        public void Stop(){
            animPlaying = false;
            animFinished = true;
        }

        public void SetDelay(float startDelay)
        {
            if (startDelay < 0f)
            {
                Debug.Log("Anim delay cannot be smaller than 0");
                return;
            }

            delay = startDelay;
            _delay = delay;
        }

        public void SetSpeed(float spd = 1.0f)
        {
            if (spd == 0f)
            {
                Debug.Log("Anim speed cannot be 0");
                return;
            }

            speed = spd;
            _speed = speed;
        }

        public void SetDuration(float dura)
        {
            if (dura <= 0f)
            {
                Debug.Log("Anim duration cannot be smaller than 0");
                return;
            }

            duration = dura;
            _duration = duration;
        }

        protected virtual void InitProps()
        {
            startPlayAnim = false;
            animPlaying = false;
            animPaused = false;
            animFinished = false;

            deltaTime = 0f;
            deltaVal = 0f;
            delayDeltaTime = 0f;

            // 動畫作用的物件, 若為空值則預設為母物件
            if (targetObj == null)
                targetObj = this.gameObject.transform.parent.gameObject;

            // 開始延遲不能小於0
            if (delay < 0f)
                delay = 0f;
            _delay = delay;

            // 播放速度不能為0, 但可以為負
            if (speed == 0f)
                speed = 1f;
            _speed = speed;

            // 動畫時間不能小於等於0
            if (duration <= 0f)
                duration = 1f;
            _duration = duration;

            // 如果動畫變量曲線的點少於兩個(代表無法連成一個線段), 則預設給一個曲線
            if (easingCurve.keys.Length <= 1)
                easingCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
        }

        // 播放動畫function, 基層類別只做基本的時間加總與變量計算, 動畫內容則是繼承的類別根據功能去實作
        protected virtual void PlayAnim()
        {
            deltaTime += Time.deltaTime / _duration * _speed;
            deltaVal = easingCurve.Evaluate(deltaTime);
            if (playReverse)
                deltaVal = 1f - deltaVal;

            if (deltaTime >= 1f && playPingPong)
            {
                deltaTime = 1f;
                _speed = -_speed;
            }
        }

        // 動畫更新迴圈
        protected virtual void Update()
        {
            if (animFinished == false && animPlaying == false && (playOnAwake || startPlayAnim))
            {
                if (delayDeltaTime >= _delay)
                    animPlaying = true;
                else
                    delayDeltaTime += Time.deltaTime;
            }

            if (animFinished == false && animPaused == false && animPlaying)
            {
                PlayAnim();

                if (deltaTime >= 1f)
                {
                    if (!playPingPong)
                    {
                        Stop();
                    }
                }
                else if (deltaTime <= 0f)
                {
                    if (playPingPong)
                    {
                        Stop();
                    }
                }
            }
        }
    }

}