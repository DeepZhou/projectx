﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using DStudio.Animator;

namespace DStudio.Animator {

    [CustomEditor(typeof(AnimProcHandler))]
    public class AnimProcHandlerEditor : Editor {

        ReorderableList reorderableList;

        private AnimProcHandler animHandler {
            get
            {
                return target as AnimProcHandler;
            }
        }

        void OnEnable() {
            if (animHandler != null)
            {
                reorderableList = new ReorderableList(animHandler.animControlItemList, typeof(AnimControlItem), true, true, true, true);

                reorderableList.drawHeaderCallback += DrawHeader;
                reorderableList.drawElementCallback += DrawElement;

                reorderableList.onAddCallback += AddItem;
                reorderableList.onRemoveCallback += RemoveItem;
            }
        }

        void OnDisable() {
            if (reorderableList != null)
            {
                reorderableList.drawHeaderCallback -= DrawHeader;
                reorderableList.drawElementCallback -= DrawElement;

                reorderableList.onAddCallback -= AddItem;
                reorderableList.onRemoveCallback -= RemoveItem;
            }
        }

        void DrawHeader(Rect rect) {
            GUI.Label(rect, "Animation Controller List");
        }

        void DrawElement(Rect rect, int idx, bool isActive, bool isFocused) {
            AnimControlItem item = animHandler.animControlItemList[idx];
    
            EditorGUI.BeginChangeCheck();
    
            // Play label
            GUI.Label(new Rect(rect.x, rect.y, 28, rect.height), "Play");
    
            // Play toggle
            item.play = EditorGUI.Toggle(new Rect(rect.x + 28, rect.y, 48, rect.height), item.play);
    
            // Controller game object reference
            item.animController = (GameObject)EditorGUI.ObjectField(new Rect(rect.x + 48, rect.y, rect.width - 48, rect.height - 5), item.animController, typeof(GameObject), true);
    
            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(target);
            }
        }

        private void AddItem(ReorderableList list)
        {
            AnimControlItem item = new AnimControlItem();
            animHandler.animControlItemList.Add(item);

            EditorUtility.SetDirty(target);
        }

        private void RemoveItem(ReorderableList list)
        {
            animHandler.animControlItemList.RemoveAt(list.index);

            EditorUtility.SetDirty(target);
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            // Actually draw the list in the inspector
            reorderableList.DoLayoutList();
        }
    }

}