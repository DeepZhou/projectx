﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DStudio.Animator {
    
    public class TransformAnim : AnimBase {
        
        public bool useNowPosAsStart;
        public Vector3 posFrom;
        public Vector3 posTo;

        public bool useNowRotAsStart;
        public Vector3 rotFrom;
        public Vector3 rotTo;

        public bool useNowScaleAsStart;
        public Vector3 scaleFrom;
        public Vector3 scaleTo;

        Vector3 _posFrom;
        Vector3 _posTo;

        Vector3 _rotFrom;
        Vector3 _rotTo;

        Vector3 _scaleFrom;
        Vector3 _scaleTo;

        protected override void InitProps()
        {
            base.InitProps();

            _posFrom = (useNowPosAsStart) ? this.gameObject.transform.localPosition : posFrom;
            _posTo = posTo;

            _rotFrom = (useNowRotAsStart) ? this.gameObject.transform.localRotation.eulerAngles : rotFrom;
            _rotTo = rotTo;

            _scaleFrom = (useNowScaleAsStart) ? this.gameObject.transform.localScale : scaleFrom;
            _scaleTo = scaleTo;
        }
            
        protected override void PlayAnim()
        {
            base.PlayAnim();

            if (deltaVal <= 1f)
            {
                targetObj.transform.localPosition = Vector3.Lerp(_posFrom, _posTo, deltaVal);
                targetObj.transform.localRotation = Quaternion.Euler(Vector3.Lerp(_rotFrom, _rotTo, deltaVal));
                targetObj.transform.localScale = Vector3.Lerp(_scaleFrom, _scaleTo, deltaVal);
            }
            else
                Stop();
        }
    }

}