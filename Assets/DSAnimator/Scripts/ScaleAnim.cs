﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DStudio.Animator {
    
    public class ScaleAnim : AnimBase {
        
        public bool useNowScaleAsStart;
        public Vector3 scaleFrom;
        public Vector3 scaleTo;

        Vector3 _scaleFrom;
        Vector3 _scaleTo;

        protected override void InitProps()
        {
            base.InitProps();

            _scaleFrom = (useNowScaleAsStart) ? targetObj.transform.localScale : scaleFrom;
            _scaleTo = scaleTo;
        }

        protected override void PlayAnim()
        {
            base.PlayAnim();

            if (deltaVal <= 1f)
                targetObj.transform.localScale = Vector3.Lerp(_scaleFrom, _scaleTo, deltaVal);
            else
                Stop();
        }
    }

}