﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace DStudio.Animator {

    public class GraphicItem {
        public Graphic targetImg;
        public float alphaFrom;
        public float alphaTo;

        public GraphicItem(Graphic targetImg, float alphaFrom, float alphaTo)
        {
            this.targetImg = targetImg;
            this.alphaFrom = alphaFrom;
            this.alphaTo = alphaTo;
        }
    }

    public class GraphicAlphaAnim : AnimBase {

        public bool useNowAlphaAsStart;
        public float alphaFrom;
        public float alphaTo;
        public List<Graphic> exceptionList;

        List<GraphicItem> targetImgList;

        protected override void InitProps()
        {
            base.InitProps();

            if (targetImgList == null)
                targetImgList = new List<GraphicItem>();
            else
                targetImgList.Clear();

            // 取得目標物件身上的Graphic元件
            foreach (var g in targetObj.GetComponents<Graphic>())
            {
                if (useNowAlphaAsStart)
                {
                    targetImgList.Add(new GraphicItem(g, g.color.a, alphaTo));
                }
                else
                {
                    targetImgList.Add(new GraphicItem(g, alphaFrom, alphaTo));
                }
            }

            // 取得目標子物件身上的Graphic元件
            foreach (var g in targetObj.GetComponentsInChildren<Graphic>())
            {
                if (useNowAlphaAsStart)
                {
                    targetImgList.Add(new GraphicItem(g, g.color.a, alphaTo));
                }
                else
                {
                    targetImgList.Add(new GraphicItem(g, alphaFrom, alphaTo));
                }
            }

            // 最後剔除掉exception清單的物件
            targetImgList.RemoveAll(x => exceptionList.Exists(y => x.targetImg == y));
        }

        protected override void PlayAnim()
        {
            base.PlayAnim();

            if (deltaVal <= 1f)
            {
                foreach (var it in targetImgList)
                {
                    Color col = it.targetImg.color;
                    col.a = Mathf.Lerp(it.alphaFrom, it.alphaTo, deltaVal);
                    it.targetImg.color = col;
                }
            }
            else
                Stop();
        }
    }

}