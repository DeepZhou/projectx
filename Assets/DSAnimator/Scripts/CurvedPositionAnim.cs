﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DStudio.Animator {

    public class CurvedPositionAnim : AnimBase {
        
        public BezierCurve positionCurve;

        Vector3 targetObjStartPos;

        protected override void InitProps()
        {
            base.InitProps();

            targetObjStartPos = targetObj.transform.localPosition;
        }

        protected override void PlayAnim()
        {
            base.PlayAnim();

            targetObj.transform.localPosition = transform.InverseTransformPoint(positionCurve.GetPointAt(deltaVal)) + targetObjStartPos;
        }
    }

}