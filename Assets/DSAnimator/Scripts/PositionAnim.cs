﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DStudio.Animator {

    public class PositionAnim : AnimBase {
        
        public bool useNowPosAsStart;
        public Vector3 posFrom;
        public Vector3 posTo;

        Vector3 _posFrom;
        Vector3 _posTo;

        protected override void InitProps()
        {
            base.InitProps();

            _posFrom = (useNowPosAsStart) ? targetObj.transform.localPosition : posFrom;
            _posTo = posTo;
        }

        protected override void PlayAnim()
        {
            base.PlayAnim();

            if (deltaVal <= 1f)
                targetObj.transform.localPosition = Vector3.Lerp(_posFrom, _posTo, deltaVal);
            else
                Stop();
        }
    }

}