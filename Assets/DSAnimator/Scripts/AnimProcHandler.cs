﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DStudio.Animator {
    
    [System.Serializable]
    public class AnimControlItem {
        public bool play;
        public GameObject animController;
    }

    public class AnimProcHandler : MonoBehaviour {

        public bool playOnAwake;

        [HideInInspector]
        public List<AnimControlItem> animControlItemList = new List<AnimControlItem>();   // 掛有複數動畫控制的物件清單

        AnimBase[] playingHndArr; // 目前正在表演的動畫控制項目, 播放前須更新下方的引數, 而後更新此陣列才能正確地按照順序播放

        bool startPlayAnim;

        int playingControlItemIdx;  // 目前播放的是第幾個動畫控制物件

        void Start() {
            // 受管理的動畫控制初始關閉"playOnAwake"這個屬性
            for (int i = 0; i < animControlItemList.Count; i++)
            {
                foreach (var animHnd in animControlItemList[i].animController.GetComponents<AnimBase>())
                {
                    animHnd.playOnAwake = false;
                }
            }

            if (playOnAwake)
                Play();
        }

        public void Play()
        {
            if (startPlayAnim)
                return;

            InitProps();
            UpdatePlayingControlList();

            for (int i=0; i<playingHndArr.Length; i++)
            {
                playingHndArr[i].Play();
            }

            startPlayAnim = true;
        }

        void InitProps()
        {
            startPlayAnim = false;
            playingControlItemIdx = 0;
        }

        void UpdatePlayingControlList()
        {
            if (playingControlItemIdx >= animControlItemList.Count)
                return;

            if (animControlItemList[playingControlItemIdx].play == false)
            {
                UpdatePlayingControlList();
            }
            else
            {
                playingHndArr = animControlItemList[playingControlItemIdx].animController.GetComponents<AnimBase>();
            }
        }

        void Update() {
            if (startPlayAnim)
            {
                bool isAllAnimFinished = true;
                for (int i=0; i<playingHndArr.Length; i++)
                {
                    if (playingHndArr[i].AnimFinished == false)
                    {
                        isAllAnimFinished = false;
                        break;
                    }
                }

                if (isAllAnimFinished)
                {
                    if (playingControlItemIdx + 1 >= animControlItemList.Count)
                    {
                        startPlayAnim = false;
                    }
                    else
                    {
                        playingControlItemIdx++;
                        UpdatePlayingControlList();
                        for (int i=0; i<playingHndArr.Length; i++)
                        {
                            playingHndArr[i].Play();
                        }
                    }
                }
            }
        }
    }

}