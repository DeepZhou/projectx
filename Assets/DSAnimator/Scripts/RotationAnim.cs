﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DStudio.Animator {

    public class RotationAnim : AnimBase {
        
        public bool useNowRotAsStart;
        public Vector3 rotFrom;
        public Vector3 rotTo;

        Vector3 _rotFrom;
        Vector3 _rotTo;

        protected override void InitProps()
        {
            base.InitProps();

            _rotFrom = (useNowRotAsStart) ? targetObj.transform.localRotation.eulerAngles : rotFrom;
            _rotTo = rotTo;
        }

        protected override void PlayAnim()
        {
            base.PlayAnim();

            if (deltaVal <= 1f)
                targetObj.transform.localRotation = Quaternion.Euler(Vector3.Lerp(_rotFrom, _rotTo, deltaVal));
            else
                Stop();
        }
    }

}