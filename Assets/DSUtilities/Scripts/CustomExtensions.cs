﻿using System;
using UnityEngine;

static class EnumExtensions
{
    ///////////////////////////////////////
    /// 針對 多重狀態的enum增加的函式
    /// 範例:
    /// enum e;
    /// e = e.Add(enum.b);
    /// if (e.Has(enum.b))
    ///     e = e.Remove(enum.b);
    ///////////////////////////////////////
    //checks if the value contains the provided type
    public static bool Has<T>(this Enum type, T value)
    {
        try
        {
            return (((int)(object)type & (int)(object)value) == (int)(object)value);
        }
        catch
        {
            return false;
        }
    }

    //checks if the value is only the provided type
    public static bool Is<T>(this Enum type, T value)
    {
        try
        {
            return (int)(object)type == (int)(object)value;
        }
        catch
        {
            return false;
        }
    }

    //appends a value
    public static T Add<T>(this Enum type, T value)
    {
        try
        {
            return (T)(object)(((int)(object)type | (int)(object)value));
        }
        catch (Exception ex)
        {
            throw new ArgumentException(
                string.Format(
                    "Could not append value from enumerated type '{0}'.",
                    typeof(T).Name
                    ), ex);
        }
    }

    //completely removes the value
    public static T Remove<T>(this Enum type, T value)
    {
        try
        {
            return (T)(object)(((int)(object)type & ~(int)(object)value));
        }
        catch (Exception ex)
        {
            throw new ArgumentException(
                string.Format(
                    "Could not remove value from enumerated type '{0}'.",
                    typeof(T).Name
                    ), ex);
        }
    }

}

static partial class TransformExtension
{
    public static RectTransform GetRectTransform(this Transform t)
    {
        return t as RectTransform;
    }
}