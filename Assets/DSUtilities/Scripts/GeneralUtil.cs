﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// DStudio Utilities
namespace DStudio.Utilities
{

    public enum LogColor
    {
        Black,
        Blue,
        Brown,
        Green,
        Grey,
        Orange,
        Purple,
        Red,
        Silver,
        White,
        Yellow
    }

    // General Utilities
    public class GeneralUtil
    {

        // Need add "DS_DEBUG" symbol in the player settings
        public static void Log(LogColor? color, params object[] msg)
        {
#if DS_DEBUG
            if (msg.Length <= 0)
                return;

            // if color is null, set black as default color
            string colorCode = (color != null) ? color.ToString() : "black";
            string finalMsg = msg[0].ToString();

            for (int i=1; i<msg.Length; i++)
            {
                finalMsg += ", " + msg[i];
            }

            Debug.Log("<color=" + colorCode + ">" + finalMsg + "</color>");
#endif
        }

        public static void Log(params object[] msg)
        {
#if DS_DEBUG
            if (msg.Length <= 0)
                return;
            
            string finalMsg = msg[0].ToString();

            for (int i=1; i<msg.Length; i++)
            {
                finalMsg += " " + msg[i];
            }

            Debug.Log("<color=blue>" + finalMsg + "</color>");
#endif
        }

        // According to time zone, return a int var with legnth 10
        public static int GetNowTimestamp()
        {
            DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            int ret = (int)(DateTime.UtcNow - epochStart).TotalSeconds;
            return ret;
        }

        // ===========================
        // File functions
        // ===========================
        #region File functions
        public static bool IsFileExist(string path)
        {
            bool isExist = File.Exists(path);
            return isExist;
        }

        public static bool IsDirectoryExist(string path)
        {
            bool isExist = Directory.Exists(path);
            return isExist;
        }

        public static void CreateFolder(string path)
        {
            bool isExist = IsDirectoryExist(path);
            if (isExist == false)
                Directory.CreateDirectory(path);
        }

        public static string ReadTextFile(string path, Encoding encode)
        {
            if (IsFileExist(path) == false)
                return null;

            StreamReader sr = new StreamReader(path, encode);
            string ret = sr.ReadToEnd();
            sr.Dispose();
            sr = null;
            return ret;
        }

        public static void WriteTextFile(string path, string content, Encoding encode, bool isAppend = false)
        {
            StreamWriter sw = null;
            if (IsFileExist(path) == false)
            {
                int lastSepCharIdx = path.LastIndexOf(Path.DirectorySeparatorChar);
                string folderPath = path.Substring(0, lastSepCharIdx);
                Directory.CreateDirectory(folderPath);
                sw = new StreamWriter(File.Open(path, FileMode.Create), encode);
                sw.Dispose();
                sw = null;
            }

            FileMode mode = FileMode.Open;
            if (isAppend)
                mode = FileMode.Append;

            sw = new StreamWriter(new FileStream(path, mode, FileAccess.Write), encode);
            sw.Write(content);
            sw.Dispose();
            sw = null;
        }

        public static void CreateFile(string path, byte[] bytes)
        {
            if (IsFileExist(path) == false)
                return;

            //            File.WriteAllBytes(path, bytes);

            FileStream fs;
            fs = File.Create(path);
            fs.Write(bytes, 0, path.Length);
            fs.Close();
            fs.Dispose();
            fs = null;
        }

        public static void DeleteFile(string path)
        {
            if (IsFileExist(path) == false)
                return;

            File.Delete(path);
        }

        public static void DeleteFolder(string path)
        {
            if (IsDirectoryExist(path) == false)
                return;

            Directory.Delete(path);
        }
        #endregion

        // ===========================
        // PlayerPrefs functions
        // ===========================
        #region Playerprefs functions
        public static string GetPlayerPrefStr(string key)
        {
            if (PlayerPrefs.HasKey(key))
            {
                return PlayerPrefs.GetString(key);
            }
            else
            {
                return null;
            }
        }

        public static void SetPlayerPrefStr(string key, string val, bool save = false)
        {
            PlayerPrefs.SetString(key, val);

            if (save)
                PlayerPrefs.Save();
        }

        public static int GetPlayerPrefInt(string key)
        {
            if (PlayerPrefs.HasKey(key))
            {
                return PlayerPrefs.GetInt(key);
            }
            else
            {
                return 0;
            }
        }

        public static void SetPlayerPrefInt(string key, int val, bool save = false)
        {
            PlayerPrefs.SetInt(key, val);

            if (save)
                PlayerPrefs.Save();
        }

        public static float GetPlayerPrefFloat(string key)
        {
            if (PlayerPrefs.HasKey(key))
            {
                return PlayerPrefs.GetFloat(key);
            }
            else
            {
                return 0f;
            }
        }

        public static void SetPlayerPrefInt(string key, float val, bool save = false)
        {
            PlayerPrefs.SetFloat(key, val);

            if (save)
                PlayerPrefs.Save();
        }

        public static void DeletePlayerPrefVal(string key)
        {
            if (PlayerPrefs.HasKey(key))
                PlayerPrefs.DeleteKey(key);
        }
        #endregion


        // Lerp animation method, update every frame with 0-1 changing value
        // ==================================================================
        // 210115 update: add delay param for start waiting for seconds
        // and fix some calculation to prevent from bugs
        // ==================================================================
        public static IEnumerator LerpAnimation(float delay, float duration, System.Action<float> updateAction, System.Action completeAction = null)
        {
            if (delay > 0f)
                yield return new WaitForSeconds(delay);

            if (duration > 0f)
            {
                float deltaTime = 0f;

                while (deltaTime < 1f)
                {
                    deltaTime += Time.deltaTime / duration;
                    updateAction?.Invoke(deltaTime);
                    yield return null;
                }

                updateAction?.Invoke(1f);
            }

            completeAction?.Invoke();
        }

    }

}