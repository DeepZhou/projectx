﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DStudio.Utilities;
using System;

public enum ParamsTypeDropdownEnum
{
    None = 0,
    Int,
    Float,
    Char,
    String,
    Bool
}

public class DSDebugGUI : MonoBehaviour {

    public bool isShowAtBegin;

    public GameObject canvas;
    public InputField gameObjectNameInputField;
    public InputField funcNameInputField;
    public InputField parametersInputField;
    public Dropdown paramsDropdown;

    bool debugModeOn;
    bool tryingToOpenDebugMode;   // 嘗試開啟debug模式

    float nextKeyTimeInterval = 2f;

    int nowKeyIdx;
    float deltaTime;

    const string OpenDebugModeHotKeyString = "dsdebug"; // 開啟debug模式所需要輸入的字串(祕技)
    char[] debugModeKeyChars;

    #region Public functions
    public void SwitchDebugMode(bool isOn)
    {
        if (debugModeOn == isOn)
            return;

        debugModeOn = isOn;
        canvas.SetActive(isOn);
    }

    public void CallFuncByString()
    {
        if (gameObjectNameInputField.text.Length <= 0)
        {
            Debug.Log("Input target game object name first");
            return;
        }

        if (funcNameInputField.text.Length <= 0)
        {
            Debug.Log("Input function name first");
            return;
        }

        GameObject target = GameObject.Find(gameObjectNameInputField.text);

        if (target == null)
        {
            Debug.Log("Cannot find target game object");
        }
        else
        {
            switch ((ParamsTypeDropdownEnum)paramsDropdown.value)
            {
                case ParamsTypeDropdownEnum.None:
                    target.SendMessage(funcNameInputField.text);
                    break;
                case ParamsTypeDropdownEnum.Int:
                    target.SendMessage(funcNameInputField.text, int.Parse(parametersInputField.text));
                    break;
                case ParamsTypeDropdownEnum.Float:
                    target.SendMessage(funcNameInputField.text, float.Parse(parametersInputField.text));
                    break;
                case ParamsTypeDropdownEnum.Char:
                    target.SendMessage(funcNameInputField.text, char.Parse(parametersInputField.text));
                    break;
                case ParamsTypeDropdownEnum.String:
                    target.SendMessage(funcNameInputField.text, parametersInputField.text);
                    break;
                case ParamsTypeDropdownEnum.Bool:
                    target.SendMessage(funcNameInputField.text, bool.Parse(parametersInputField.text));
                    break;
            }
        }
    }
    #endregion

    void InitProps()
    {
        tryingToOpenDebugMode = false;

        if (isShowAtBegin)
        {
            debugModeOn = true;
            canvas.SetActive(true);
        }
        else
        {
            debugModeOn = false;
            canvas.SetActive(false);
        }

        nowKeyIdx = 0;
        deltaTime = 0f;

        if (FindObjectsOfType<EventSystem>().Length >= 2)
        {
            Destroy(GetComponentInChildren<EventSystem>().gameObject);
        }

        paramsDropdown.onValueChanged.AddListener(OnParamsTypeDropdownChanged);
    }

    void OnParamsTypeDropdownChanged(int idx)
    {
        if (idx == 0)
        {
            parametersInputField.interactable = false;
        }
        else
        {
            parametersInputField.interactable = true;
        }
    }

    #region Mono functions
    void Awake() {
        debugModeKeyChars = new char[OpenDebugModeHotKeyString.Length];

        for (int i = 0; i < OpenDebugModeHotKeyString.Length; i++)
        {
            debugModeKeyChars[i] = OpenDebugModeHotKeyString[i];
        }
    }

    void Start() {
        InitProps();
    }

    void Update() {
        #if UNITY_EDITOR
        if (tryingToOpenDebugMode)
        {
            if (nowKeyIdx >= debugModeKeyChars.Length)
            {
                tryingToOpenDebugMode = false;
                nowKeyIdx = 0;
                SwitchDebugMode(!debugModeOn);
                return;
            }

            if (Input.GetKeyDown(debugModeKeyChars[nowKeyIdx].ToString()))
            {
                nowKeyIdx++;
                deltaTime = 0f;
                GeneralUtil.Log("Debug mode trying! Now index: " + nowKeyIdx);
            }

            deltaTime += Time.deltaTime;

            if (deltaTime >= nextKeyTimeInterval)
            {
                tryingToOpenDebugMode = false;
                nowKeyIdx = 0;
                GeneralUtil.Log("Debug mode trying overtime, reset!");
            }
        }
        else
        {
            if (Input.GetKeyDown(debugModeKeyChars[nowKeyIdx].ToString()))
            {
                tryingToOpenDebugMode = true;
                nowKeyIdx++;
                deltaTime = 0f;
                GeneralUtil.Log("Debug mode trying!");
            }
        }
        #endif
    }
    #endregion

    #region Test methods
    public void NoParamsFunc()
    {
        Debug.Log("Hi, I'm FuncNoParams");
    }

    public void CharFunc(char param)
    {
        Debug.Log("Hi, I'm CharFunc, arg: " + param);
    }

    public void StrFunc(string param)
    {
        Debug.Log("Hi, I'm StrFunc, arg: " + param);
    }

    public void IntFunc(int param)
    {
        Debug.Log("Hi, I'm IntFunc, arg: " + param);
    }

    public void FloatFunc(float param)
    {
        Debug.Log("Hi, I'm FloatFunc, arg: " + param);
    }

    public void BoolFunc(bool param)
    {
        Debug.Log("Hi, I'm BoolFunc, arg: " + param);
    }
    #endregion
}